# README #

IRIS Detection and processing app.
- flask server deployed on AWS
- IOS app for the front-end

### Implementation ###

- Detect face from iPhone or web Camera

- Detect the iris from the face

- If the size of iris is enough large then obfuscate processing 
   else message it is safe for internet


### Dependencies ###

* opencv3.2.0
* dlib19.0.4
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)